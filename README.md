### Teste FullStack Pleno
Este teste consiste em entendermos um pouco mais sobre seus conhecimentos com HTML5, CSS3, JavaScript, PHP e arquitetura cliente servidor.

### Orientações

##### Crie uma API Restfull com recurso de posts e as ações de listar, adicionar e atualizar baseada no diagrama abaixo. 

 - Deve ser possível identificar a aplicação cliente que está consumindo o recurso;

##### Crie uma aplicação cliente para gerenciar o recurso de posts da API.

 - Deve ter uma interface de autenticação dos "users";
 - Deve ter uma interface restrita a usuários autenticados com caracteristicas de painel administrativo com uma tabela de posts, um formulário para cadastro e edição.

##### Crie uma aplicação cliente para consumir o recurso de posts da API.

 - Deve ter uma interface com caracteristicas de um blog com página de posts(apenas posts publicados) e outra com detalhes de um post

![Alt text](database.png?raw=true "Sugestão para estrutura do banco de dados")

### Diferenciais

1. Organização da estrutura;
2. Usar bibliotecas específicas e não frameworks;
3. Infraestrutura dockerizada; 
4. Qualidade(código intelegível e escalável) e versionamento do código;
5. Testes automátizados;
6. Funcionalidades extras como filtros de posts, pesquisas, etc... 
7. Design atrativo, profissional e com técnicas de UX/UI;
8. Tempo de conclusão.

** Disponibilizar o projeto em um repositório do Github com instruções básicas de como instalar e rodar em ambiente local.

** Notificar no email processoseletivo@rockbuzz.com.br